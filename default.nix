{
  sources ? import ./nix/sources.nix # See: https://github.com/nmattia/niv and https://nix.dev/tutorials/towards-reproducibility-pinning-nixpkgs.html#dependency-management-with-niv
, pkgs ? import sources.nixpkgs {}   # Use the pinned sources.
}:

with pkgs;

stdenv.mkDerivation {
  name = "md-paper";
  buildInputs = [
    (texlive.combine {
      inherit (texlive) scheme-basic
      latexmk beamer cryptocode mathtools xargs xkeyval forloop pbox varwidth
      bigfoot environ booktabs csquotes pgfplots subfig caption pgfgantt biblatex
      biber easy-todo tocloft invoice bxcalc fp siunitx xstring acronym;
    })
    glibcLocales
  ];
  src = ./.;
  buildPhase = "make";

  meta = with lib; {
    description = "Mutual Disclosure Paper";
    platform = [ platforms.linux platforms.darwin ];
  };
}
